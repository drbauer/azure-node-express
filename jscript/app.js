// drbauer 2016-1228
// A simplified Express server.  This project isa test bed
// for designing the ST2 v 6.0 line of products.
var express = require('express');
var app = express();
var path = require('path');
// Allow loading static files from the /public folder:
app.use(express.static('public'));
router();
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Bad request!');
});
//-----------------------------------------------
// Routes
function router() {
    // Respond with index.html on the homepage:
    app.get('/', function (req, res) {
        //res.send('Hello World!');
        res.sendFile(path.join(__dirname, '/public', 'Index.html'));
    });
    // Respond with 'Test: Hi to you also.' on the homepage:
    app.get('/test', function (req, res) {
        res.send('Test: Hi to you also.');
    });
    // Show general help 
    app.get('/help', function (req, res) {
        res.send(helper.General());
    });
    // Show the azure directions page
    app.get('/help/WebApp', function (req, res) {
        res.send(helper.WebApp());
    });
    // Show the bitbucket push directions page
    app.get('/help/Bitbucket', function (req, res) {
        res.sendFile(path.join(__dirname, '/public', 'bitbucketpush.html'));
    });
    // Respond with Hello World! on the homepage:
    app.get('/user/try/to/test', function (req, res) {
        res.send('Test: OK> We will try.');
    });
    // Respond to POST request on the root route (/), the application’s home page:
    app.post('/', function (req, res) {
        res.send('Got a POST request');
    });
    // Respond to a PUT request to the /user route:
    app.put('/user', function (req, res) {
        res.send('Got a PUT request at /user');
    });
    // Respond to a DELETE request to the /user route:
    app.delete('/user', function (req, res) {
        res.send('Got a DELETE request at /user');
    });
}
//-----------------------------------------------
// Start listening:
app.listen(3000, function () {
    console.log('Listening on port 3000.');
});
//-----------------------------------------------
//Catch-all error handler overide:
function errorHandler(err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500);
    res.render('error', { error: err });
}
// Help System:
var helper = (function () {
    return {
        WebApp: function () {
            return AzureWebAppHelp;
        },
        General: function () {
            var msg = '<pre>This is some help<br />'
                + 'You can also enter "help/WebApp" or "help Bitbucket".<br /></pre>';
            return msg;
        }
    };
})();
//-----------------------------------------------
// Generate Azure Web Service Instructions:
var AzureWebAppHelp = (function () {
    var doclink = 'https://docs.microsoft.com/en-us/azure/app-service-web/web-sites-nodejs-develop-deploy-mac';
    var msg = 'How to create an Azure Web App<br />'
        + '<pre>Source: BIGMAC/.../Users/Dev/Projects/express/myapp/app.js<br />'
        + 'Date: 2016-1227 - 17:37<br /></pre>';
    var deploy = '<pre>'
        + '1. git init (while in the project dir)<br />'
        + '   Create some source (this file)<br />'
        + '2. git add .<br />'
        + '3. git commit -m  "First commit to our Azure Web App."<br />'
        + '   One Time Setup -> <a href=' + doclink + '> see ' + doclink + '</a><br />'
        + '4. git remote add azure https://drbauer@st2-proto.scm.azurewebsites.net:443/ST2-Proto.git><br />'
        + '5. Publish: git push azure master <br />'
        + '   Repeat steps 3 & 5 to commit and publish as needed.<br /></pre>';
    var htm = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"'
        + '"http://www.w3.org/TR/html4/strict.dtd">'
        + '<html lang="en">'
        + '<head>'
        + '<meta http-equiv="content-type" content="text/html; charset=utf-8">'
        + '<title>Dons Azure Node WebApp</title>'
        + '</head>'
        + '<body>'
        + '<h1>Azure Web App Help</h1>'
        + msg
        + '<h3>Steps:</h3>'
        + deploy
        + '</body>'
        + '</html>';
    return htm;
})();
//# sourceMappingURL=app.js.map
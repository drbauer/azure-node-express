//-----------------------------------------------
// router(): Setup the REST (Representational state transfer) API routes
function setRoutes(app) {
	// Respond with index.html on the homepage:
	app.get('/', function (req, res) {
		//res.send('Hello World!');
		res.sendFile(path.join(__dirname, '/public', 'Index.html'));
	});

	// Respond with 'Test: Hi to you also.' on the homepage:
	app.get('/test', function (req, res) {
		res.send('Test: Hi to you also.');
	});

	// Show general help 
	app.get('/help', function (req, res) {
		res.send(helper.General());
	});

	// Show the azure directions page
	app.get('/help/WebApp', function (req, res) {
		res.send(helper.WebApp());
	});

	// Show the bitbucket push directions page
	app.get('/help/Bitbucket', function (req, res) {
		res.sendFile(path.join(__dirname, '/public', 'bitbucketpush.html'));
	});

	// Respond with Hello World! on the homepage:
	app.get('/user/try/to/test', function (req, res) {
		res.send('Test: OK> We will try.');
	});

	// Respond to POST request on the root route (/), the application’s home page:
	app.post('/', function (req, res) {
		res.send('Got a POST request')
	});

	// Respond to a PUT request to the /user route:
	app.put('/user', function (req, res) {
		res.send('Got a PUT request at /user')
	});

	// Respond to a DELETE request to the /user route:
	app.delete('/user', function (req, res) {
		res.send('Got a DELETE request at /user')
	});

	
}

export { setRoutes };
